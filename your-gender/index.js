// When page is loaded
$(document).ready(function(){
  // When button is clicked with id of 'submit_button'
  $('#submit_button').click(function() {
    // Get value of the input
    var name = $('#name_input').val();
    // Request the gender of the name
    $.ajax({
      url: 'https://api.genderize.io/?name=' + name,
      success: function(data){
        // Alert the result
        alert(data.name + ' is a ' + data.gender);
      }
    });
  });
});
