// When page is loaded
$(document).ready(function(){
  // Request quote of the day
  $.ajax({
    url: 'http://quotes.rest/qod.json',
    success: function(data){
      var quote = data.contents.quotes[0].quote;
      var author = data.contents.quotes[0].author;
      // Show quote inside of the tag with id of 'quote'
      $('#quote').html(quote);
      // Show author inside of the tag with id of 'author'
      $('#author').html('-' + author);
    }
  });
});
