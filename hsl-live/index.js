// When page is loaded
$(document).ready(function(){
  // Create the map
  var map = L.map('map').setView([60.192059,24.945831], 11);
  // Add CC-BY-SA tag
  L.tileLayer('http://api.digitransit.fi/map/v1/{id}/{z}/{x}/{y}.png', {
    attribution: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, <a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>',
    id: 'hsl-map'
  }).addTo(map);
  // Request live vehicle positions
  $.ajax({
    url: 'http://api.digitransit.fi/realtime/vehicle-positions/v1/hfp/journey/',
    success: function(data){
      // For each vehicle
      Object.keys(data).forEach(function(id){
        var vehicle = data[id].VP;
        // Create icon with vehicle line name
        var icon = new L.DivIcon({
          className: 'icon',
          html: '<span>' + vehicle.desi +  '</span>'
        });
        // Add marker to map
        L.marker([vehicle.lat, vehicle.long], {
          icon: icon
        }).addTo(map).bindPopup('<h2>' + vehicle.desi + '</h2><p><b>Speed:</b> ' + vehicle.spd + ' km/h</p>');
      });
    }
  });
});
